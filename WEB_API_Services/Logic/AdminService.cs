﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.Helpers;
using WEB_API_Entities.ViewModels;
using WEB_API_Repositories.Interfaces;
using WEB_API_Services.Interfaces;

namespace WEB_API_Services.Logic
{
    public class AdminService : IAdminService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGroupsRepository _groupsRepository;
        private readonly IUsersGroupsRepository _usersGroupsRepository;

        public AdminService(UserManager<ApplicationUser> userManager, IGroupsRepository groupsRepository, IUsersGroupsRepository usersGroupsRepository)
        {
            _userManager = userManager;
            _groupsRepository = groupsRepository;
            _usersGroupsRepository = usersGroupsRepository;
        }

        #region Users
        public async Task<IdentityResult> UpdateUser(UpdateUserVM updatedUser)
        {
            var appUser = await _userManager.FindByNameAsync(updatedUser.OldEmail);

            if(appUser == null)
            {
                return null;
            }

            if (appUser.Email != updatedUser.Email)
            {
                appUser.Email = updatedUser.Email;
                appUser.UserName = updatedUser.Email;
            }
            appUser.FirstName = updatedUser.FirstName;
            appUser.LastName = updatedUser.LastName;
            appUser.Gender = updatedUser.Gender;
            appUser.Birthday = updatedUser.Birthday;

            var updateResult = await _userManager.UpdateAsync(appUser);
            return updateResult;
        }

        public async Task<List<UsersInfo>> GetAllUsers(string currUserEmail)
        {
            var users = await _userManager.Users.ToListAsync();
            var usersInfoList = users.Where(x => x.Email != currUserEmail).Select(appUser => {
                var usersInfo = Mappers.ApplicationUserToUsersInfo.CreateMapper().Map<UsersInfo>(appUser);
                var roles = _userManager.GetRolesAsync(appUser).GetAwaiter().GetResult();
                usersInfo.Role = roles.First();
                return usersInfo;
            }).ToList();
            return usersInfoList;
        }

        public async Task<IdentityResult> RemoveUser(string userEmail)
        {
            var appUser = await _userManager.FindByNameAsync(userEmail);
            if (appUser == null)
            {
                return null;
            }

            var userInGroups = await _usersGroupsRepository.GetRowsByUser(appUser.Id);
            var aaa = userInGroups.ToList();
            var groups = userInGroups.Select(x => x.Groups).ToList();
            foreach(var gr in groups)
            {
                gr.UserCount -= 1;
                var newGr = await _groupsRepository.EditGroup(gr);
            }
            if (userInGroups != null && userInGroups.Count() > 0)
            {
                await _usersGroupsRepository.RemoveRows(userInGroups);
                
            }

            return await _userManager.DeleteAsync(appUser);
        }

        public async Task<IdentityResult> ChangeUserRole(ChangeUserRoleVM chnageUserRoleVM)
        {
            var appUser = await _userManager.FindByNameAsync(chnageUserRoleVM.UserEmail);
            if(appUser != null)
            {
                var roles = await _userManager.GetRolesAsync(appUser);
                var res1 = await _userManager.RemoveFromRolesAsync(appUser, roles);
                if(res1.Succeeded)
                {
                    return await _userManager.AddToRoleAsync(appUser, chnageUserRoleVM.UserRole);
                }
                else
                {
                    return res1;
                }
            } else
            {
                return null;
            }
        }
        #endregion

        #region Groups
        public async Task<List<Group>> GetAllGroups()
        {
            var groups = await _groupsRepository.GetAllGroups();
            return groups;
        }
        public async Task<Group> CreateGroup(Group newGroup)
        {
            var group = await _groupsRepository.CreateGroup(newGroup);
            return group;
        }

        public async Task<Group> EditGroup(Group newGroup)
        {
            var group = await _groupsRepository.EditGroup(newGroup);
            return group;
        }

        public async Task<UsersInGroupVM> GetUsersInGroup(int grId)
        {
            var usersInGroupVM = new UsersInGroupVM();
            usersInGroupVM.AllUser = await GetAllUsers(null);
            var users = await _usersGroupsRepository.GetUsersInGroup(grId);
            var emails = users.Select(x => x.Users.Email).ToList();
            usersInGroupVM.UsersInGroup = emails;
            return usersInGroupVM;
        }

        public async Task<int> ChangeUsersInGroup(int groupId, List<string> userEmails)
        {
            var users = await _userManager.Users.ToListAsync();
            var usersForGroup = users.Where(x => userEmails.Contains(x.Email)).ToList();
            var gr = await _groupsRepository.GetGroupById(groupId);
            gr.UserCount = usersForGroup.Count;
            await _usersGroupsRepository.RemoveRows(groupId);
            var usersGropList = usersForGroup.Select(x => new UsersGroups() { Groups = gr, Users = x }).ToList();
            await _usersGroupsRepository.AddUserToGroupMany(usersGropList);
            var editedGr = await _groupsRepository.EditGroup(gr);
            //var res = await _usersGroupsRepository.GetUsersInGroup(groupId);
            return editedGr.UserCount;
        }

        public async Task<Group> RemoveGroup(Group gr)
        {
            var usersInGroup = await _usersGroupsRepository.GetUsersInGroup(gr.Id);
            if(usersInGroup != null && usersInGroup.Count > 0)
            {
                await _usersGroupsRepository.RemoveRows(gr.Id);
            }

            var res = await _groupsRepository.RemoveGroup(gr);

            return res;
        }
        #endregion
    }
}
