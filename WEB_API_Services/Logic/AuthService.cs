﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.Helpers;
using WEB_API_Entities.LoginModels;
using WEB_API_Entities.LoginModels.Helpers;
using WEB_API_Entities.LoginModels.JWT;
using WEB_API_Entities.ViewModels;
using WEB_API_Repositories.Interfaces;
using WEB_API_Services.Interfaces;

namespace WEB_API_Services.Logic
{
    public class AuthService : IAuthService
    {
      //  private readonly IAuthRepository _authRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;
        //private readonly IMapper _mapper;
        public AuthService(UserManager<ApplicationUser> userManager, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions)
        {
            //_authRepository = authRepository;
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions.Value;
        }


        private async Task<JWTModel> GetJwtToken(ApplicationUser identityUser)
        {
            var roles = await _userManager.GetRolesAsync(identityUser);
            string role;
            if (roles.Count > 1 && roles.Contains(Constants.Strings.Roles.Admin))
            {
                role = Constants.Strings.Roles.Admin;
            }
            else
            {
                role = roles.First();
            }
            var identity = _jwtFactory.GenerateClaimsIdentity(identityUser.Email, identityUser.Id, role);

            var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, identityUser.Email, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });

            return jwt;
        }

        public async Task<ApplicationUser> GetUserFromDB(string email)
        {
            return await _userManager.FindByNameAsync(email);
        }

        public async Task<bool> CheckUserPassword(ApplicationUser appUser, string password)
        {
            return await _userManager.CheckPasswordAsync(appUser, password);
        }

        public async Task<RegisteredUserVW> GetAuthorizedUser(ApplicationUser appUser)
        {
            var registeredUser = Mappers.ApplicationUserToRegisteredUser.CreateMapper().Map<RegisteredUserVW>(appUser);
            registeredUser.Token = await GetJwtToken(appUser);
            return registeredUser;
        }

        public async Task<RegisteredUserVW> GetUserByEmail(string email)
        {
            var appUser = await GetUserFromDB(email);
            var registeredUser = await GetAuthorizedUser(appUser);
            return registeredUser;
        }

        public async Task<ApplicationUser> Login(LoginVM loginObj)
        {
            //return await _authRepository.LoginUser(loginObj.Email, loginObj.Password);

            return null;
        }

        public async Task<RegisteredUserVW> Registration(NewUserVM userModel)
        {
            var appUser = await GetUserFromDB(userModel.Email);

            if (appUser == null)
            {
                var user = Mappers.NewUserToApplicationUser.CreateMapper().Map<ApplicationUser>(userModel);

                var res = await _userManager.CreateAsync(user, userModel.Password);
                if (res.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "User");
                    var registeredUser = await GetAuthorizedUser(user);
                    return registeredUser;
                }
                else
                {
                    return null;
                }                

            } else
            {
                var registeredUser = await GetAuthorizedUser(appUser);
                return registeredUser;         
            }
        }
    }
}
