﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.Helpers;
using WEB_API_Entities.ViewModels;
using WEB_API_Services.Interfaces;

namespace WEB_API_Services.Logic
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<ApplicationUser> UpdateUser(RegisteredUserVW userVW)
        {
            var appUser =  await _userManager.FindByNameAsync(userVW.Email);

            appUser.FirstName = userVW.FirstName;
            appUser.LastName = userVW.LastName;

            var updateResult = await _userManager.UpdateAsync(appUser);
            if(updateResult.Succeeded)
            {
                return appUser;
            } else
            {
                return null;
            }
        }

        public async Task<IdentityResult> ChangePassword(ChangePasswordVM changePassword)
        {
            var appUser = await _userManager.FindByNameAsync(changePassword.userEmail);
            if (appUser == null)
            {
                return null;
            }
            return await _userManager.ChangePasswordAsync(appUser, changePassword.oldPassword, changePassword.newPassword);
        }
    }
}
