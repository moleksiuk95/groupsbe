﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.LoginModels;
using WEB_API_Entities.ViewModels;

namespace WEB_API_Services.Interfaces
{
    public interface IAuthService
    {
        //Task<User> Login(LoginVM loginObj);
        Task<ApplicationUser> GetUserFromDB(string email);
        Task<RegisteredUserVW> GetUserByEmail(string email);
        Task<bool> CheckUserPassword(ApplicationUser appUser, string password);
        Task<RegisteredUserVW> Registration(NewUserVM user);
        Task<RegisteredUserVW> GetAuthorizedUser(ApplicationUser appUser);
    }
}
