﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.ViewModels;

namespace WEB_API_Services.Interfaces
{
    public interface IAdminService
    {
        #region Users
        Task<IdentityResult> UpdateUser(UpdateUserVM updatedUser);
        Task<List<UsersInfo>> GetAllUsers(string currUserEmail);
        Task<IdentityResult> RemoveUser(string userEmail);
        Task<IdentityResult> ChangeUserRole(ChangeUserRoleVM chnageUserRoleVM);
        #endregion

        #region Groups
        Task<List<Group>> GetAllGroups();
        Task<Group> CreateGroup(Group newGroup);
        Task<Group> EditGroup(Group newGroup);
        Task<UsersInGroupVM> GetUsersInGroup(int grId);
        Task<int> ChangeUsersInGroup(int groupId, List<string> userEmails);
        Task<Group> RemoveGroup(Group gr);
        #endregion
    }
}
