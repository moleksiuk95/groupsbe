﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.ViewModels;

namespace WEB_API_Services.Interfaces
{
    public interface IUserService
    {
        Task<ApplicationUser> UpdateUser(RegisteredUserVW userVW);
        Task<IdentityResult> ChangePassword(ChangePasswordVM changePassword);
    }
}
