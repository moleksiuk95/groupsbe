﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB_API.Controllers.Api
{
    [Route("api")]
    [ApiController]
    
    public class IndexController : ControllerBase
    {
        [HttpGet("index")]
        public ActionResult<IEnumerable<string>> Index()
        {
            return new string[] { "INDEX" };
        }
       
    }
}
