﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.ViewModels;
using WEB_API_Services.Interfaces;

namespace WEB_API.Controllers.Api.v1
{
    [Route("api/v1/admin")]
    [ApiController]
    [Authorize(Policy = "Admin", AuthenticationSchemes = "Bearer")]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        #region Users
        [HttpPost("user-update")]
        public async Task<IActionResult> UpdateUser(UpdateUserVM user)
        {
            var res = await _adminService.UpdateUser(user);
            return Ok(user);
        }

        [HttpGet("user-list")]
        public async Task<IActionResult> GetUsers(string userEmail)
        {
            var res = await _adminService.GetAllUsers(userEmail);
            return Ok(res);
        }

        [HttpDelete("remove-user")]
        public async Task<IActionResult> RemoveUser(string userEmail)
        {
            var res = await _adminService.RemoveUser(userEmail);
            if(res == null)
            {
                return BadRequest();
            }
            if(res.Succeeded)
            {
                return Ok();
            } else
            {
                return BadRequest(res.Errors.First().Description);
            }
        }

        [HttpPost("change-role")]
        public async Task<ActionResult> ChangeUserRole(ChangeUserRoleVM chnageUserRoleVM)
        {
            var res = await _adminService.ChangeUserRole(chnageUserRoleVM);
            if(res == null)
            {
                return BadRequest();
            }
            if(res.Succeeded)
            {
                return Ok(chnageUserRoleVM);
            } else
            {
                return BadRequest(res.Errors.First().Description);
            }
        }
        #endregion

        #region Groups
        [HttpGet("get-groups")]
        public async Task<IActionResult> GetAllGroups()
        {
            var res = await _adminService.GetAllGroups();
            return Ok(res);
        }

        [HttpPost("create-group")]
        public async Task<IActionResult> CreateGroup(Group newGroup)
        {
            var res = await _adminService.CreateGroup(newGroup);
            return Ok(res);
        }

        [HttpPost("edit-group")]
        public async Task<IActionResult> EditGroup(Group newGroup)
        {
            var res = await _adminService.EditGroup(newGroup);
            return Ok(res);
        }

        [HttpGet("users-in-group")]
        public async Task<IActionResult> GetUsersInGroup(string grId)
        {
            var res = await _adminService.GetUsersInGroup(int.Parse(grId));
            return Ok(res);
        }

        [HttpPost("change-users-in-group")]
        public async Task<IActionResult> ChangeUsersInGroup(KeyValuePair<int, List<string>> updateObj)
        {
            var count = await _adminService.ChangeUsersInGroup(updateObj.Key, updateObj.Value);
            return Ok(count);
        }

        [HttpPost("remove-group")]
        public async Task<IActionResult> RemoveGroup(Group removingGroup)
        {
            var res = await _adminService.RemoveGroup(removingGroup);
            return Ok(res);
        }
        #endregion
    }
}
