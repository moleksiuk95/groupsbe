﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB_API_Entities.LoginModels;
using WEB_API_Entities.ViewModels;
using WEB_API_Services.Interfaces;

namespace WEB_API.Controllers.Api.v1
{
    [Route("api/v1/user")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public UserController(IUserService userService, IAuthService authService)
        {
            _userService = userService;
            _authService = authService;
        }

        [Authorize(Policy = "User", AuthenticationSchemes = "Bearer")]
        [HttpPost("update")]
        public async Task<IActionResult> UpdateUser(RegisteredUserVW user)
        {
            var updatedUser = await _userService.UpdateUser(user);
            if(updatedUser != null)
            {
                var registeredUser = await _authService.GetAuthorizedUser(updatedUser);
                return Ok(registeredUser);
            } else
            {
                return BadRequest("Can't update user");
            }
        }

        [HttpPost("password-check")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> CheckPassword(LoginVM login)
        {
            var appUser = await _authService.GetUserFromDB(login.Email);
            if(appUser == null)
            {
                return BadRequest("User not found");
            }
            var passwordIsCorrect = await _authService.CheckUserPassword(appUser, login.Password);
            if(passwordIsCorrect)
            {
                return Ok(true);
            }
            else
            {
                return BadRequest("Password is incorrect");
            }
        }

        [HttpPost("change-password")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> ChangePassword(ChangePasswordVM changePassword)
        {
            var result = await _userService.ChangePassword(changePassword);
            if(result == null)
            {
                return BadRequest("User not found");
            }
            else
            {
                if(result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(result.Errors.First().Description);
                }
            }
        }

    }
}
