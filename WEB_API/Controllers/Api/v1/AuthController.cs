﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using WEB_API_Entities.CustomErrors;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.LoginModels;
using WEB_API_Entities.LoginModels.Helpers;
using WEB_API_Entities.LoginModels.JWT;
using WEB_API_Entities.ViewModels;
using WEB_API_Services.Interfaces;

namespace WEB_API.Controllers.Api.v1
{
    [Route("api/v1/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginVM loginData)
        {
            try
            {
                var identityUser = await _authService.GetUserFromDB(loginData.Email);

                if(identityUser == null)
                {
                    return BadRequest(new { message = "User not found" });
                }

                var passwordIsValid = await _authService.CheckUserPassword(identityUser, loginData.Password);
                if (!passwordIsValid)
                {
                    return BadRequest(new { message = "Username or password is incorrect" });
                }

                var authorizedUser = await _authService.GetAuthorizedUser(identityUser);

                return Ok(authorizedUser);

            } catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message});
            }

        }

        [HttpPost("registry")]
        [AllowAnonymous]
        public async Task<IActionResult> Registry(NewUserVM user)
        {
            try
            {
                if(user.Password != user.ConfirmPassword)
                {
                    throw new PasswordsNotMatchException();
                }
                var registeredUser = await _authService.Registration(user);
                if(registeredUser != null)
                {
                    return Ok(registeredUser);
                }                    
                else
                {
                    return BadRequest("Can't create user!");
                }
            }
            catch(Exception e)
            {
                return BadRequest(new {  e.Message, e.StackTrace});
            }
             
        }

        [HttpGet("get-user")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetUserByEmail(string userEmail)
        {
            var user = await _authService.GetUserByEmail(userEmail);
            return Ok(user);
        }

        [Authorize(Policy = "Admin", AuthenticationSchemes = "Bearer")]
        [HttpGet("test-controller")]
        public async Task<IActionResult> Test()
        {
            return Ok("Hellow admin");
        }

        [Authorize(Policy = "User", AuthenticationSchemes = "Bearer")]
        [HttpGet("test-controller-user")]
        public async Task<IActionResult> UserTest()
        {
            return Ok("Hellow user");
        }
    }
}