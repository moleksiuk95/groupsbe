﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WEB_API_Entities.CustomErrors
{
    public class PasswordsNotMatchException : Exception
    {
        public PasswordsNotMatchException() : base("'Password' and 'Confirm password' do not match") { }
        public PasswordsNotMatchException(string message) : base(message) { }

    }

}
