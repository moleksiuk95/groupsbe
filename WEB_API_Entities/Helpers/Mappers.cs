﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.ViewModels;

namespace WEB_API_Entities.Helpers
{
    public static class Mappers
    {
        public static MapperConfiguration ApplicationUserToRegisteredUser = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, RegisteredUserVW>());
        public static MapperConfiguration ApplicationUserToUsersInfo = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, UsersInfo>());

        public static MapperConfiguration NewUserToApplicationUser = new MapperConfiguration(cfg => cfg.CreateMap<NewUserVM, ApplicationUser>()
                .ForMember("UserName", opt => opt.MapFrom(o => o.Email))
                .ForMember("SecurityStamp", opt => opt.MapFrom(o => Guid.NewGuid().ToString())));
        public static MapperConfiguration RegisteredUserToApplicationUser = new MapperConfiguration(cfg => cfg.CreateMap<RegisteredUserVW, ApplicationUser>()
                .ForMember("UserName", opt => opt.MapFrom(o => o.Email)));
    }
}
