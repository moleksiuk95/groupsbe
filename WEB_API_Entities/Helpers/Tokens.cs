﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.LoginModels;
using WEB_API_Entities.LoginModels.JWT;

namespace WEB_API_Entities.Helpers
{
    public class Tokens
    {
        public static async Task<JWTModel> GenerateJwt(ClaimsIdentity identity, IJwtFactory jwtFactory, string userName, JwtIssuerOptions jwtOptions, JsonSerializerSettings serializerSettings)
        {
            var response = new JWTModel
            {
                Id = identity.Claims.Single(c => c.Type == "id").Value,
                Auth_token = await jwtFactory.GenerateEncodedToken(userName, identity),
                Expires_in = (int)jwtOptions.ValidFor.TotalSeconds
            };

            return response;
        }
    }
}
