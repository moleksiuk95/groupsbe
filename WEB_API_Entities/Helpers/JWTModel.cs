﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WEB_API_Entities.Helpers
{
    public class JWTModel
    {
        public string Id { get; set; }
        [JsonProperty("authToken")]
        public string Auth_token { get; set; }
        [JsonProperty("expiresIn")]
        public int Expires_in { get; set; }
    }
}
