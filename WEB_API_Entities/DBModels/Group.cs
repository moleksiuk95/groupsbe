﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WEB_API_Entities.DBModels
{
    public class Group
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string GroupName { get; set; }
        [StringLength(500, ErrorMessage = "Max langth of fielad = 500 charcters")]
        public string GroupDescr { get; set; }
        public int UserCount { get; set; }
    }
}
