﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WEB_API_Entities.Enums;

namespace WEB_API_Entities.DBModels
{
    public class ApplicationUser : IdentityUser
    {
        //[Required(ErrorMessage = "First name is required"), StringLength(127, ErrorMessage = "Max langth of fielad = 127 charcters")]
        public string FirstName { get; set; }
        //[Required(ErrorMessage = "Last name is required"), StringLength(127, ErrorMessage = "Max langth of fielad = 127 charcters")]
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public GenderEnum Gender { get; set; } = GenderEnum.Man;
        public string Photo { get; set; }
    }
}
