﻿using System;
using System.Collections.Generic;
using System.Text;
using WEB_API_Entities.DBModels;

namespace WEB_API_Entities.ViewModels
{
    public class NewUserVM : ApplicationUser
    {
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
