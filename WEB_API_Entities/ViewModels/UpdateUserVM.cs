﻿using System;
using System.Collections.Generic;
using System.Text;
using WEB_API_Entities.Enums;

namespace WEB_API_Entities.ViewModels
{
    public class UpdateUserVM
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string OldEmail { get; set; }
        public DateTime Birthday { get; set; }
        public GenderEnum Gender { get; set; }
    }
}
