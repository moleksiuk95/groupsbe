﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WEB_API_Entities.ViewModels
{
    public class ChangeUserRoleVM
    {
        public string UserEmail { get; set; }
        public string UserRole { get; set; }
    }
}
