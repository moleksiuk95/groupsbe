﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WEB_API_Entities.ViewModels
{
    public class ChangePasswordVM
    {
        public string userEmail { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
    }
}
