﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WEB_API_Entities.ViewModels
{
    public class UsersInGroupVM
    {
        public List<UsersInfo> AllUser { get; set; }
        public List<string> UsersInGroup { get; set; }
    }
}
