﻿using System;
using System.Collections.Generic;
using System.Text;
using WEB_API_Entities.DBModels;
using WEB_API_Entities.Enums;
using WEB_API_Entities.Helpers;

namespace WEB_API_Entities.ViewModels
{
    public class RegisteredUserVW
    {
        public string FirstName { get; set; }
        //[Required(ErrorMessage = "Last name is required"), StringLength(127, ErrorMessage = "Max langth of fielad = 127 charcters")]
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public GenderEnum Gender { get; set; } = GenderEnum.Man;
        public string Photo { get; set; }
        public JWTModel Token { get; set; }
    }
}
