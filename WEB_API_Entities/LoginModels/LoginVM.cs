﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WEB_API_Entities.LoginModels
{
    public class LoginVM
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
