﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WEB_API_Entities.LoginModels.Helpers
{
    public static class Constants1
    {
        public static class Strings
        {
            public static class JwtClaimIdentifiers
            {
                public const string Rol = "rol", Id = "id";
            }

            public static class Roles
            {
                public const string Admin = "Admin";
                public const string User = "User";
            }
        }
    }
}
