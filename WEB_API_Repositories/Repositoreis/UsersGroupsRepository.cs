﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBContext;
using WEB_API_Entities.DBModels;
using WEB_API_Repositories.Interfaces;

namespace WEB_API_Repositories.Repositoreis
{
    public class UsersGroupsRepository : IUsersGroupsRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public UsersGroupsRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task AddUserToGroup(UsersGroups usersGroup)
        {
            throw new NotImplementedException();
        }

        public async Task AddUserToGroupMany(List<UsersGroups> usersGroup)
        {
            await _dbContext.UsersGroups.AddRangeAsync(usersGroup);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<UsersGroups> GetUsersGroupsByGroupId(int groupId)
        {
            return _dbContext.UsersGroups.FirstOrDefault(x => x.Groups.Id == groupId); 
        }

        public async Task<List<UsersGroups>> GetUsersInGroup(int groupId)
        {
            var res = _dbContext.UsersGroups.Where(x => x.Groups.Id == groupId).ToList();
            return res;
        }

        public async Task RemoveRows(int grId)
        {
            var rows = _dbContext.UsersGroups.Where(x => x.Groups.Id == grId).ToList();
            _dbContext.UsersGroups.RemoveRange(rows);
            await _dbContext.SaveChangesAsync();
        }

        public async Task RemoveRows(IEnumerable<UsersGroups> usersInGroups)
        {
            _dbContext.UsersGroups.RemoveRange(usersInGroups);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<UsersGroups>> GetRowsByUser(string userId)
        {
            var rows = _dbContext.UsersGroups.Where(x => x.Users.Id == userId).Select(x => new UsersGroups() { Groups = x.Groups, Id = x.Id, Users = x.Users });
            return rows;
        }
    }
}
