﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBContext;
using WEB_API_Entities.DBModels;
using WEB_API_Repositories.Interfaces;

namespace WEB_API_Repositories.Repositoreis
{
    public class GroupsRepository : IGroupsRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public GroupsRepository(ApplicationDbContext dbContext) {
            _dbContext = dbContext;
        }
        public async Task<List<Group>> GetAllGroups()
        {
            var list = _dbContext.Groups.ToList();
            return list;
        }

        public async Task<Group> CreateGroup(Group newGroup)
        {
            var result = await _dbContext.Groups.AddAsync(newGroup);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<Group> EditGroup(Group newGroup)
        {
            var result = _dbContext.Groups.Update(newGroup);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<Group> GetGroupById(int grId)
        {
            var gr = _dbContext.Groups.FirstOrDefault(x => x.Id == grId);
            return gr;
        }

        public async Task<Group> RemoveGroup(Group gr)
        {
            var result = _dbContext.Groups.Remove(gr);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

    }
}
