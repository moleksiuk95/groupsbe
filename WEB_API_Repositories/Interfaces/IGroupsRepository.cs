﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;

namespace WEB_API_Repositories.Interfaces
{
    public interface IGroupsRepository
    {
        Task<List<Group>> GetAllGroups();
        Task<Group> CreateGroup(Group newGroup);
        Task<Group> EditGroup(Group newGroup);
        Task<Group> GetGroupById(int grId);
        Task<Group> RemoveGroup(Group gr);
    }
}
