﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;

namespace WEB_API_Repositories.Interfaces
{
    public interface IAuthRepository
    {
        Task<ApplicationUser> LoginUser(string email, string password);
       // Task RegisterUser(ApplicationUser user);

    }
}
