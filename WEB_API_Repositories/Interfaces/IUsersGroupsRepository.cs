﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WEB_API_Entities.DBModels;

namespace WEB_API_Repositories.Interfaces
{
    public interface IUsersGroupsRepository
    {
        Task<List<UsersGroups>> GetUsersInGroup(int groupId);
        Task AddUserToGroup(UsersGroups userGroup);
        Task AddUserToGroupMany(List<UsersGroups> userGroup);
        Task RemoveRows(int grId);
        Task RemoveRows(IEnumerable<UsersGroups> usersInGroups);
        Task<IEnumerable<UsersGroups>> GetRowsByUser(string userId);
    }
}
